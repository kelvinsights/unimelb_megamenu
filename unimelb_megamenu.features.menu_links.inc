<?php
/**
 * @file
 * unimelb_megamenu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function unimelb_megamenu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-mega-menu_media-releases:http://newsroom.melbourne.edu/news
  $menu_links['menu-mega-menu_media-releases:http://newsroom.melbourne.edu/news'] = array(
    'menu_name' => 'menu-mega-menu',
    'link_path' => 'http://newsroom.melbourne.edu/news',
    'router_path' => '',
    'link_title' => 'MEDIA RELEASES',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => 'Latest news',
      ),
      'identifier' => 'menu-mega-menu_media-releases:http://newsroom.melbourne.edu/news',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 0,
  );
  // Exported menu link: menu-mega-menu_studio:http://newsroom.melbourne.edu/studio
  $menu_links['menu-mega-menu_studio:http://newsroom.melbourne.edu/studio'] = array(
    'menu_name' => 'menu-mega-menu',
    'link_path' => 'http://newsroom.melbourne.edu/studio',
    'router_path' => '',
    'link_title' => 'STUDIO',
    'options' => array(
      'attributes' => array(),
      'alter' => TRUE,
      'identifier' => 'menu-mega-menu_studio:http://newsroom.melbourne.edu/studio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 0,
  );
  // Exported menu link: menu-mega-menu_up-close:http://upclose.unimelb.edu.au
  $menu_links['menu-mega-menu_up-close:http://upclose.unimelb.edu.au'] = array(
    'menu_name' => 'menu-mega-menu',
    'link_path' => 'http://upclose.unimelb.edu.au',
    'router_path' => '',
    'link_title' => 'UP CLOSE',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => 'Podcast',
      ),
      'identifier' => 'menu-mega-menu_up-close:http://upclose.unimelb.edu.au',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 0,
  );
  // Exported menu link: menu-mega-menu_visions:http://visions.unimelb.edu.au
  $menu_links['menu-mega-menu_visions:http://visions.unimelb.edu.au'] = array(
    'menu_name' => 'menu-mega-menu',
    'link_path' => 'http://visions.unimelb.edu.au',
    'router_path' => '',
    'link_title' => 'VISIONS',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => 'Videos',
      ),
      'identifier' => 'menu-mega-menu_visions:http://visions.unimelb.edu.au',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 0,
  );
  // Exported menu link: menu-mega-menu_voice:http://voice.unimelb.edu.au
  $menu_links['menu-mega-menu_voice:http://voice.unimelb.edu.au'] = array(
    'menu_name' => 'menu-mega-menu',
    'link_path' => 'http://voice.unimelb.edu.au',
    'router_path' => '',
    'link_title' => 'VOICE',
    'options' => array(
      'attributes' => array(
        'title' => 'Monthly publication',
      ),
      'alter' => TRUE,
      'identifier' => 'menu-mega-menu_voice:http://voice.unimelb.edu.au',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('MEDIA RELEASES');
  t('STUDIO');
  t('UP CLOSE');
  t('VISIONS');
  t('VOICE');


  return $menu_links;
}
