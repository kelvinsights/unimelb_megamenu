<?php
/**
 * @file
 * unimelb_megamenu.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function unimelb_megamenu_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Business and Economics',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 1,
    'uuid' => '02c69ff7-4117-48be-806d-70cd44ee88f1',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Science and the Environments',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 7,
    'uuid' => '2e9ee04f-cebf-45fc-9b40-837d92bcc879',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Education',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 6,
    'uuid' => '525f9bb1-05a8-40ab-ac20-478a8a080e74',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Law',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 0,
    'uuid' => '5a6acbf8-009b-46ed-b165-cc34dd96a567',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Engineering and IT',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 3,
    'uuid' => '6a00e0e2-87d7-4b73-81ed-bbaef7803429',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Art, Culture and Society',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 10,
    'uuid' => '7e0054ec-2591-4102-bd86-57c97f973b07',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'History and Politics',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 11,
    'uuid' => '9c25dff7-c1c0-4fd7-8c2b-fef945f44624',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Sport',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 4,
    'uuid' => 'abe24521-0552-411e-90fb-dbd728e18e36',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Architecture, Design and Construction',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 2,
    'uuid' => 'b4d62a9e-88bf-4062-b6cc-102c5d71bff8',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'University Activities',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 5,
    'uuid' => 'c4f3d036-2377-4691-ba0e-eb7351703106',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'International',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 12,
    'uuid' => 'c925717f-85c7-4769-8c79-70eb7f84c69a',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Music and Performing Arts',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 9,
    'uuid' => 'd19c3b90-d0f0-4a08-9e5a-c22d23efdf5d',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Medicine and Health Science',
    'description' => '',
    'format' => 'rich_text',
    'weight' => 8,
    'uuid' => 'd4452fbd-fe04-41af-bf0b-e9f7762b35b8',
    'vocabulary_machine_name' => 'topics',
    'metatags' => array(),
  );
  return $terms;
}
