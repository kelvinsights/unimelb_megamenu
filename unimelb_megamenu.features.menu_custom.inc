<?php
/**
 * @file
 * unimelb_megamenu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function unimelb_megamenu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-mega-menu.
  $menus['menu-mega-menu'] = array(
    'menu_name' => 'menu-mega-menu',
    'title' => 'Mega menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Mega menu');


  return $menus;
}
