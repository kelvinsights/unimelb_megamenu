jQuery(document).ready(function(){

  (function($) {
    $("a.title.Media.Releases").html(function(){
      return '<i class="icon megamenu icon-news" aria-hidden="true"></i>' + $(this).text();
    });

    $("a.title.Up.Close").html(function(){
      return '<i class="icon megamenu icon-audio" aria-hidden="true"></i>' + $(this).text();
    });

    $("a.title.Visions").html(function(){
      return '<i class="icon megamenu icon-video" aria-hidden="true"></i>' + $(this).text();
    });

    $("a.title.Voice").html(function(){
      return '<i class="icon megamenu icon-voice" aria-hidden="true"></i>' + $(this).text();
    });

    $("a.title.Studio").html(function(){
      return '<i class="icon megamenu icon-video" aria-hidden="true"></i>' + $(this).text();
    });

  })(jQuery);

});
