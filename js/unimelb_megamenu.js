jQuery(document).ready(function(){

  (function($) {

    $("#mega-tabs").tabs("div.mega-tabs-panel");


    //record the initial active panel and tab
    currentPanel=$(".mega-tabs-panel").filter(":visible").attr('id');
    var parentClass = $("#mega-tabs li a").filter(".current").parent().attr('class');
    currentTab = parentClass? parentClass.split(' ')[0]: '';

    //Hide all tabs and panels initially
    $(".mega-tabs-panel").hide();
    $("#mega-tabs li a").removeClass('current');



    //Close panel
    $(".mega-tabs-panel a.close").click(function () {
      //remove active class from mega-tabs
      $("#mega-tabs ul").removeClass('active');


      //record the active panel and tab (if there is an active one)
      if ($(".mega-tabs-panel").filter(":visible").attr('id')){
        currentPanel=$(".mega-tabs-panel").filter(":visible").attr('id');
        currentTab=$("#mega-tabs li a").filter(".current").parent().attr('class').split(' ')[0];
      }

      //Hide all tabs and panels
      $(".mega-tabs-panel").slideUp();
      $("#mega-tabs li a").removeClass('current');

      $(this).hide();

      return false;
    })


    //Restory the active tab when mouse enter
    $("#mega-tabs li a").click(function() {

      //add active class from mega-tabs
      $("#mega-tabs ul").addClass('active');

      //Restory the active tab when mouse enter
      if ($(this).parent().hasClass(currentTab)){
        $("#"+currentPanel).show();
        $(this).addClass('current');
      }

      //show all close button
      $(".mega-tabs-panel a.close").show();

    });


  })(jQuery);

});
